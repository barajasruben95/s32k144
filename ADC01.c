#include "S32K144.h"

int main(void)
{
	uint32_t result;

	//Habilitar reloj.
	PCC -> PCCn[PCC_PORTD_INDEX] = (1<<30);
	PCC -> PCCn[PCC_ADC0_INDEX] = (1<<30) | (3<<24);

	//Pines de los LEDs en modo GPIO.
	PORTD->PCR[0] = (1<<8);
	PORTD->PCR[15] = (1<<8);
	PORTD->PCR[16] = (1<<8);

	//LEDs apagados. Logica invertida.
	PTD->PSOR = 1 | (1<<15) | (1<<16); //LED azul, rojo y verde respectivamente.

	//Pines de los LEDs como salida.
	PTD->PDDR = 1 | (1<<15) | (1<<16);

	//Configuracion ADC a 10 bits.
	ADC0->CFG1 = (2<<2);
	SCG->FIRCDIV = (1<<8); //Encender reloj del FIRDIV.

	for(;;)
	{
		//Arrancar ADC canal 12.
		ADC0->SC1[0] = 12;

		//Esperar conversion.
		while((1<<7) != (ADC0->SC1[0] & (1<<7)));

		result = ADC0->R[0] * 5;

		PTD->PSOR = 1 | (1<<15) | (1<<16);
		if(result < 1250)
			asm("nop");
		else if(result > 1249 && result < 2500)
			PTD->PCOR = 1;
		else if(result > 2499 && result < 3750)
			PTD->PCOR = (1<<16);
		else
			PTD->PCOR = (1<<15);
	}

	return 0;
}
