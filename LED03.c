/*
    Programa que le aplica toogle al LED blanco cada 500ms por interrupción del LPTMR0.
*/

#include "S32K144.h"

int main(void)
{
	//Habilitar reloj.
	PCC -> PCCn[PCC_LPTMR0_INDEX] = (1<<30);
	PCC -> PCCn[PCC_PORTD_INDEX] = (1<<30);

	//Pines de los LEDs en modo GPIO.
	PORTD->PCR[0] = (1<<8);
	PORTD->PCR[15] = (1<<8);
	PORTD->PCR[16] = (1<<8);

	//LEDs apagados. Logica invertida.
	PTD->PSOR = 1 | (1<<15) | (1<<16); //LED azul, rojo y verde respectivamente.

	//Pines de los LEDs como salida.
	PTD->PDDR = 1 | (1<<15) | (1<<16);

	//Configuraciones LPTMR0.
	LPTMR0->PSR = 5;			//Configuracion del preescaler.
	LPTMR0->CMR = 500 - 1;		//Contador cada 500ms.
	LPTMR0->CSR = (1<<6) | 1;	//Habilitador local de interrupciones y habilitacion del LPTMR0.

	//Interrupciones NVIC.
	S32_NVIC->ISER[1] |= (1<<(58%32)); 	//Interrupciones LPTMR0.

	for(;;);

	return 0;
}

void LPTMR0_IRQHandler(void)
{
	//Limpiar bandera.
	LPTMR0->CSR |= (1<<7);

	//Toogle al LED blanco (suma de R, G y B).
	PTD->PTOR = 1 | (1<<15) | (1<<16);
}
