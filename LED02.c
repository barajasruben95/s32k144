#include "S32K144.h"

/*
    Programa que cambia el estado del LED cada vez que se presiona el push botton.
*/

int main(void)
{
	//Habilitar reloj.
	PCC -> PCCn[PCC_PORTC_INDEX] = (1<<30);
	PCC -> PCCn[PCC_PORTD_INDEX] = (1<<30);

	//Pin del push botton y de los LEDs en modo GPIO.
	PORTC->PCR[12] = (1<<8) | (1<<1) | 1; //Push botton con resistencia pull up.
	PORTD->PCR[0] = (1<<8);
	PORTD->PCR[15] = (1<<8);
	PORTD->PCR[16] = (1<<8);

	//LEDs apagados. Logica invertida.
	PTD->PSOR = 1 | (1<<15) | (1<<16); //LED azul, rojo y verde respectivamente.

	//Pines de los LEDs como salida.
	PTD->PDDR = 1 | (1<<15) | (1<<16);

	for(;;)
	{
		//Saber si se presiono el push botton
		if(1 == (PTC->PDIR>>12))
		{
			while(1 == (PTC->PDIR>>12));		//Esperar a que se suelte el boton.
			PTD->PTOR = 1 | (1<<15) | (1<<16);	//Toogle al LED blanco (Suma de R, G y B).
		}
	}

	return 0;
}
