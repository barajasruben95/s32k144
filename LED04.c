#include "S32K144.h"

uint8_t datoLED;

int main(void)
{
	//Habilitar reloj.
	PCC -> PCCn[PCC_PORTC_INDEX] = (1<<30);
	PCC -> PCCn[PCC_PORTD_INDEX] = (1<<30);
	PCC -> PCCn[PCC_LPTMR0_INDEX] = (1<<30);

	//Pines de los LEDs y el push botton en modo GPIO.
	PORTC->PCR[12] = (10<<16) | (1<<8) | (1<<1) | 1;	//Push botton genera interrupcion por falling edge. Pull up habilitado.
	PORTD->PCR[0] = (1<<8);
	PORTD->PCR[15] = (1<<8);
	PORTD->PCR[16] = (1<<8);

	//LEDs apagados. Logica invertida.
	PTD->PSOR = 1 | (1<<15) | (1<<16); 	//LED azul, rojo y verde respectivamente.

	//Pines de los LEDs como salida.
	PTD->PDDR = 1 | (1<<15) | (1<<16);

	//Configuraciones LPT.
	LPTMR0->PSR = 5;			//Configuracion del preescaler.
	LPTMR0->CMR = 500 - 1;			//Contador cada 500ms.
	LPTMR0->CSR = (1<<6) | 1;		//Habilitador local de interrupciones y habilitacion del LPTMR0.

	//Interrupciones NVIC.
	S32_NVIC->ISER[1] |= (1<<(61%32)); 	//Interrupciones GPIOC.
	S32_NVIC->ISER[1] |= (1<<(58%32)); 	//Interrupciones LPTMR0.

	for(;;);

	return 0;
}

void PORTC_IRQHandler(void)
{
	//Limpiar bandera.
	PORTC->PCR[12] |= (1<<24);

	datoLED^=1;
}

void LPTMR0_IRQHandler(void)
{
	//Limpiar bandera.
	LPTMR0->CSR |= (1<<7);

	//Manejo de los LEDs.
	if(0 == datoLED)
	{
		PTD->PTOR = 1;		//Toogle al LED habilitado.
		PTD->PSOR = (1<<15);
	}
	else
	{
		PTD->PSOR = 1;
		PTD->PTOR = (1<<15);	//Toogle al LED habilitado.
	}
}
