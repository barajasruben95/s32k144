# Programas básicos para la tarjeta S32K144 Evaluation Board de NXP.
Funcionamiento de los principales perifericos del microcontrolador S32K144.

## Debug en RAM.
Para poder cargar los programas en RAM con el debug será necesario aplicar las siguientes configuraciones:
  - Ir a la opción de "Debug Configurations" del IDE.
  - A la izquierda en el apartado "GDB PEMicro Interface Debugging", expandir la sección y seleccionar la opcion "ProjectName_Debug_RAM".
  - A la derecha apareceran nuevas opciones. Seleccionar la barra de herramienta "Debugger" e iniciar sesión con la cuenta de correo
    con el que se activó el IDE.
  - Una vez ingresado y verificado el correo, en la opcion "Interface" seleccionar la opción "OpenSDA Embedded Debug - USB Port" y 
    en opción "Port" deberá aparecernos algo similar a "USB - OpenSDA(xxxxxxxx)" donde x es un numero hexadecimal.
  - Aplicar cambios y dar aceptar a las siguientes ventanas.
  - Probar el debugger.
