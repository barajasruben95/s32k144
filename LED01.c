#include "S32K144.h"

/*
	Programa para encender y apagar los LEDs RGB en secuencia con cierta duracion cada uno.
*/

void delay(void)
{
	uint32_t contador = 0;
	for(contador = 0; contador < 5000000; contador++)
	{
		asm("nop");
	}
}

int main(void)
{
	//Habilitar reloj.
	PCC -> PCCn[PCC_PORTD_INDEX] = (1<<30);

	//Pines de los LEDs en modo GPIO.
	PORTD->PCR[0] = (1<<8);
	PORTD->PCR[15] = (1<<8);
	PORTD->PCR[16] = (1<<8);

	//LEDs apagados. Logica invertida.
	PTD->PSOR = 1 | (1<<15) | (1<<16); //LED azul, rojo y verde respectivamente.

	//Pines de los LEDs como salida.
	PTD->PDDR = 1 | (1<<15) | (1<<16);

	for(;;)
	{
		PTD->PSOR = 1 | (1<<15) | (1<<16);	//Apagar LEDs.
		PTD->PCOR = 1;						//Encender LED azul.
		delay();
		PTD->PSOR = 1 | (1<<15) | (1<<16);	//Apagar LEDs.
		PTD->PCOR = (1<<15);				//Encender LED rojo.
		delay();
		PTD->PSOR = 1 | (1<<15) | (1<<16);	//Apagar LEDs.
		PTD->PCOR = (1<<16);				//Encender LED verde.
		delay();
	}

	return 0;
}
