/*
    Programa que envia un string y un numero por la terminar serial LPUART1 del microcontrolador.
*/

#include "S32K144.h"

void UART_Write(uint8_t data)
{
	//Esperar a que el buffer de transmision este vacio para despues enviar.
	while((1<<22) != (LPUART1->STAT & (1<<22)));

	LPUART1->DATA = data;
}

void UART_WriteString(uint8_t *data)
{
	while (*data)
		UART_Write(*data++);
}

void UART_WriteNumber(uint32_t number)
{
	uint8_t digitos[10] = {'0','0','0','0','0','0','0','0','0','0'};
	uint8_t i;

	//Busqueda de los digitos del numero.
	i = 0;
	do
	{
		digitos[i++] = (number % 10) + '0';
		number = (int32_t)(number / 10);
	}while(number);

	//Visualizar el numero en consola
	for(i--; i != 255; i--)
		UART_Write(digitos[i]);
}

int main(void)
{
	uint8_t texto[] = {"HOLA MUNDO \n\r"};
	uint32_t contador;

	//Habilitar reloj.
	PCC -> PCCn[PCC_PORTC_INDEX] = (1<<30);

	PCC -> PCCn[PCC_LPUART1_INDEX] &= ~(1<<30);
	PCC -> PCCn[PCC_LPUART1_INDEX] |= (3<<24);
	PCC -> PCCn[PCC_LPUART1_INDEX] |= (1<<30);

	//Configuracion de la UART.
	PORTC->PCR[7] = (2<<8);
	PORTC->PCR[6] = (2<<8);
	SCG->FIRCDIV = (1<<8); 				//Encender reloj de FIRCDIV preescaler 1.
	LPUART1->BAUD |= 24;  	 			//Baudrate 115200.
	LPUART1->CTRL = (1<<18) | (1<<19);	//Habilitar recepcion-transmision.

	contador = 0;
	for(;;)
	{
		UART_WriteString(&texto[0]);
		UART_WriteNumber(contador++);
		UART_Write('\n'); UART_Write('\r');
	}

	return 0;
}
