#include "S32K144.h"

uint32_t adcResult;
uint8_t texto[] = {"X.XX\n\r"};

void UART_Write(uint8_t data)
{
	//Esperar a que el buffer de transmision este vacio para despues enviar.
	while((1<<22) != (LPUART1->STAT & (1<<22)));

	LPUART1->DATA = data;
}

void UART_WriteString(uint8_t *data)
{
	while (*data)
		UART_Write(*data++);
}

void setDigits(uint32_t number)
{
	uint8_t digitos[3] = {'0','0','0'};
	uint8_t i;

	//Busqueda de los digitos del numero.
	i = 0;
	do
	{
		digitos[i++] = (number % 10) + '0';
		number = (int32_t)(number / 10);
	}while(number);

	texto[0] = digitos[2];
	texto[2] = digitos[1];
	texto[3] = digitos[0];
}

int main(void)
{
	//Habilitar reloj.
	PCC -> PCCn[PCC_PORTC_INDEX] = (1<<30);
	PCC -> PCCn[PCC_PORTD_INDEX] = (1<<30);
	PCC -> PCCn[PCC_ADC0_INDEX] = (1<<30) | (3<<24);
	PCC -> PCCn[PCC_LPTMR0_INDEX] = (1<<30);

	PCC -> PCCn[PCC_LPUART1_INDEX] &= ~(1<<30);
	PCC -> PCCn[PCC_LPUART1_INDEX] |= (3<<24);
	PCC -> PCCn[PCC_LPUART1_INDEX] |= (1<<30);

	SCG->FIRCDIV = (1<<8); 				//Encender reloj de FIRCDIV preescaler 1.

	//Pines de los LEDs en modo GPIO.
	PORTD->PCR[0] = (1<<8);
	PORTD->PCR[15] = (1<<8);
	PORTD->PCR[16] = (1<<8);

	//LEDs apagados. Logica invertida.
	PTD->PSOR = 1 | (1<<15) | (1<<16); //LED azul, rojo y verde respectivamente.

	//Pines de los LEDs como salida.
	PTD->PDDR = 1 | (1<<15) | (1<<16);

	//Configuracion de la UART.
	PORTC->PCR[7] = (2<<8);
	PORTC->PCR[6] = (2<<8);
	LPUART1->BAUD |= 24;  	 			//Baudrate 115200.
	LPUART1->CTRL = (1<<18) | (1<<19);	//Habilitar recepcion-transmision.

	//Configuraciones LPTMR0.
	LPTMR0->PSR = 5;			//Configuracion del preescaler.
	LPTMR0->CMR = 50 - 1;		//Interrupcion cada 50ms.
	LPTMR0->CSR = (1<<6) | 1;	//Habilitador local de interrupciones y habilitacion del LPTMR0.

	//Interrupciones NVIC.
	S32_NVIC->ISER[1] |= (1<<(58%32)); 	//Interrupciones LPTMR0.
	S32_NVIC->ISER[1] |= (1<<(39%32));	//Interrupciones de ADC0.

	for(;;);

	return 0;
}

void ADC0_IRQHandler(void)
{
	adcResult = ADC0->R[0] * 2;
	setDigits(adcResult);

	UART_WriteString(&texto[0]);
}

void LPTMR0_IRQHandler(void)
{
	//Limpiar bandera.
	LPTMR0->CSR |= (1<<7);

	//Arrancar ADC canal 12 con interrupciones habilitadas.
	ADC0->SC1[0] = 12 | (1<<6);
}
